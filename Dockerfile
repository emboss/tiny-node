FROM node:lts-alpine

ENV APP_DIR /app
RUN mkdir $APP_DIR

WORKDIR $APP_DIR
COPY . .

RUN npm install

CMD npm start

