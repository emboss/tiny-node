import { describe, expect, test } from '@jest/globals'
import { sum } from '../src/index.js'

describe('sum function', () => {
  test('2 + 3 == 5', () => {
    expect(sum(2, 3)).toBe(5)
  })
  test('1 + 1 == 2', () => {
    expect(sum(1, 1)).toBe(2)
  })
})

