/**
  * Returns the sum of two numbers.
  * @param {number} a - First number contributing to the sum
  * @param {number} b - Second number contributing to the sum
  * @return {number} Sum of `a` and `b`.
  * @example
  *    sum(2, 3) => 5
  */
export function sum(a, b) {
  return a + b
}

console.log(`The sum of 2 and 3 is ${sum(2, 3)}`)

