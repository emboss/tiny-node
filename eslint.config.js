import js from "@eslint/js"

export default [
  {
    files: ["src/**/*.js"],
    ...js.configs.recommended,
    languageOptions: {
      globals: {
        console: "readonly"
      }
    }
  }
]

